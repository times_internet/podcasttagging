#!/usr/bin/env python
# coding: utf-8

# In[38]:


#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import random
from gensim.models import Word2Vec 
import copy

import warnings;
warnings.filterwarnings('ignore')

import re
import csv

from nltk.stem import WordNetLemmatizer
import nltk
nltk.download('wordnet')

import time
import datetime
start_time = time.time()
print(time.ctime())
import inflect

p = inflect.engine()


# In[39]:


record = ['show_id', 'title', 'keywords+category', 'tag_ids', 'category_ids', 'no_of_tags']
filename= 'Podcasts.csv'
filename1= 'PodcastsAll_notPredicted.csv'
with open(filename,'w+') as f:
    writer = csv.writer(f)
    writer.writerow(record)


# In[40]:



import mysql.connector

# mydb = mysql.connector.connect(
#   host="172.26.111.237",
#   user="StgPlayApp",
#   password="AppHkwH2cBf"
# )

mydb = mysql.connector.connect(
  host="172.26.116.95",
  user="RdnLYctxMus",
  password="RlUsRMzT920"
)

# today_date_string = today_date
# print(today_date_string)
# prev_date_string = prev_date.strftime('%Y-%m-%d %H:%M:%S')
    
starting_day = datetime.date.today()-datetime.timedelta(1)
starting_day = starting_day.strftime('%Y-%m-%d %H:%M:%S')
print(starting_day)
end_day = datetime.date.today()-datetime.timedelta(0)
end_day = end_day.strftime('%Y-%m-%d %H:%M:%S')
print(end_day)

crsr = mydb.cursor() 
#query = f"SELECT id, title, keywords, author, summary, all_categories, manual_categories, feed_resource FROM music.tm_podcasts_show where cast(category_upadated_on as date) between ('{starting_day}') and ('{end_day}') or  cast(created_on as date) between ('{starting_day}') and ('{end_day}');"
query = f"SELECT id, title, keywords, author, summary, all_categories, manual_categories, feed_resource FROM music.tm_podcasts_show where cast(category_upadated_on as date) between ('{starting_day}') and ('{end_day}') or  cast(created_on as date) between ('{starting_day}') and ('{end_day}');"
print(query)
crsr.execute(query)
#crsr.execute(query, (starting_day, end_day) )

df = pd.DataFrame(crsr)
df.columns= ['id', 'title', 'keywords', 'author', 'summary', 'all_categories', 'manual_categories', 'feed_resource']
print("The keywords and all_categories are")
print(df.head())
print(df.shape)



#manually adding show_ids
query = f"SELECT id, title, keywords, author, summary, all_categories, manual_categories, feed_resource FROM music.tm_podcasts_show where id in (23389, 23390);"
print(query)
crsr.execute(query)
#crsr.execute(query, (starting_day, end_day) )

df1 = pd.DataFrame(crsr)
df1.columns= ['id', 'title', 'keywords', 'author', 'summary', 'all_categories', 'manual_categories', 'feed_resource']
print("The keywords and all_categories are")
print(df1.head())
print(df1.shape)
df = df.append(df1,ignore_index = True)

# In[41]:


"""exclude_shows = pd.read_csv('Top 500 show list - Shows Tagging Completed.csv', usecols=['Show ID'])
exclude_shows_list = exclude_shows['Show ID'].tolist()

df = df[~df['id'].isin(exclude_shows_list)]
print(df.shape)"""


# In[42]:


def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3 


# In[43]:


df['all_categories'].replace('', np.nan, inplace=True)
df['all_categories'].replace(',', np.nan, inplace=True)
df_not_predicted = df
df_not_predicted = df_not_predicted[df_not_predicted['all_categories'].isna()]
#df_not_predicted = df_not_predicted.isna(subset=['all_categories'], inplace=True)
df.dropna(subset=['all_categories'], inplace=True)

#Both_DFs.dropna(subset=['keywords'], inplace=True)
print(df.shape)


# In[6]:


#tags_categories = pd.read_csv('Category-wise Tags - TagsVipul.csv')
#categories = tags_categories.columns.tolist()
crsr = mydb.cursor() 
query = f"SELECT tag_val_name, id, is_category, created_by, is_available from music.tm_tag_val_master where is_category=1 and is_available in (2,3) and is_active=1 and created_by>1"
crsr.execute(query)
#crsr.execute(query, (starting_day, end_day) )

tags_df = pd.DataFrame(crsr)
categories = tags_df.iloc[:,0].tolist()
categories = [a.lower() for a in categories]

print(categories)
all_tags = []

lemmatizer = WordNetLemmatizer() 


# In[7]:


# for column in categories:
#     all_tags.extend(tags_categories[f'{column}'].tolist())

# pure_tags = copy.copy(all_tags)
# pure_tags = [tag.lower() for tag in pure_tags if str(tag) != 'nan']
crsr = mydb.cursor() 
query = f"SELECT tag_val_name, id, is_category, created_by, is_available from music.tm_tag_val_master where is_category=0 and is_available in (2,3) and is_active=1 and created_by>1"
crsr.execute(query)
#crsr.execute(query, (starting_day, end_day) )

tags_df = pd.DataFrame(crsr)
pure_tags = tags_df.iloc[:,0].tolist()
pure_tags = [a.lower() for a in pure_tags]
all_tags.extend(pure_tags) 
all_tags.extend(categories)
all_tags = [tag.lower() for tag in all_tags if str(tag) != 'nan']
print("all_tags:", all_tags)

top_categories = categories
try:
    top_categories.remove('Category')
except Exception as e:
    pass

tag_cat = dict()
    
top_categories = [category.lower() for category in top_categories]
print(top_categories)


# In[8]:


exact_matches_only = ['ayurveda', 'buddhism', 'hundusim', 'islam', 'christianity', 'British','indian', 'mythology', 'American', 'Cricket','Baseball','Basketball','Tennis','Football','Soccer', 'Hockey','Kabaddi', 'Black','Hispanic','Asian','European','African','Chinese','SouthAmerican', 'Russian', 'Japanese', 'English', 'Hindi','Spanish', 'French', 'Chinese', 'Latin', 'fiction', 'non-fiction', 'Boxing', 'MMA', 'Cricket','Baseball', 'Basketball', 'Tennis', 'Football', 'Soccer', 'Hockey', 'Kabaddi', 'Rugby', 'Fantasy Sports', 'Golf', 'Running', 'Swimming', 'Volleyball', 'Badminton', 'Wrestling', 'Cycling', 'Crossfit', 'Bodybuilding',  'Gymnastics', 'Karate', 'Marathon', 'Snooker', 'Pool', 'Billiards', 'Chess', 'Hiking', 'Fishing', 'Triathlon', 'Decathlon', 'Shooting', 'Archery', 'Trekking', 'Weightlifting', 'Bollywood', 'Western', 'HipHop', 'rap', 'DJ', 'Remix', 'Jazz', 'Pop', 'Rock music', 'Classical', 'EDM', 'Indie', 'Indian', 'Music History', 'Punjabi Music', 'Retro', 'Kpop', 'Carnatic', 'Hindustani', 'RocknRoll', 'Bhajan', 'metal music', 'Acoustic', 'Cover Songs', 'Instrumental', 'Blues music', 'BeatBoxing', 'Country Music', 'R&B','psychedelic', 'metal', 'dance', 'Trance', 'cloud', 'torah', 'review', 'reviews', 'gadgets', 'privacy', 'Gaming', 'documentary', 'tech reviews']
#'Sports Betting',
print(len(exact_matches_only))
exact_matches_only = [a.lower() for a in exact_matches_only]
inflected_form = [p.singular_noun(word) if word.endswith('s') else p.plural_noun(word) for word in exact_matches_only]
exact_matches_only.extend(inflected_form)
exact_matches_only = list(set(exact_matches_only))
print(exact_matches_only)
exact_matches_only_dict = dict(zip(exact_matches_only, range(len(exact_matches_only))))

def match_with_keywords(redundant_tags, keywords):
    keywords_dict = dict(zip(keywords, range(len(keywords))))

    for each_tag in copy.copy(redundant_tags):
        keyword_match = True
        if each_tag in exact_matches_only_dict:
            if ' ' in each_tag:
                for each_word in each_tag.split():
                    if not each_word in keywords_dict:
                        keyword_match = False

            else:
                if not each_tag in keywords_dict:
                    redundant_tags.remove(each_tag)
            
            if keyword_match==False:
                print("tag removed: ", each_tag)
                redundant_tags.remove(each_tag)
            
    return redundant_tags      

def make_singular_plural_list(given_list):
    inflected_form = [p.singular_noun(word) if word.endswith('s') else p.plural_noun(word) for word in given_list]
    given_list.extend(inflected_form)
    return list(set(given_list))
               


# In[ ]:


motivation = ['self improvement', 'self help']

meditation = ['mindfulness, meditation']

import gensim
pre_trained_word2vec = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)
keyword_model = gensim.models.KeyedVectors.load_word2vec_format('keywords_after_splitting_word2vec.bin', binary=True)

print(f"{(time.time()-start_time)/60:.2f} mins")


# In[ ]:


all_categories = df['all_categories'].tolist()
all_categories_list = []
for category_list in all_categories:
    one_word_tags = []
    tags = (category_list.split(','))
    for tag in tags:
        if not tag.isalpha():
            one_word_tags.extend(re.sub('[^a-zA-Z]', ' ', tag).split())
        else:
            one_word_tags.append(tag)
    all_categories_list.extend(one_word_tags)
    
all_categories_list = [ a.lower().strip() for a in all_categories_list ]
all_categories_list = sorted(list((set(all_categories_list))))


# In[ ]:


ignore_hyphen = dict()
print(all_tags)
for tag in copy.copy(all_tags):
    if '-' in tag:
        print(tag)
        new_tag = re.sub('[^a-zA-Z0-9]', '', tag)
        space_separated = re.sub('[^a-zA-Z0-9]', ' ', tag)
        try:
            pre_trained_word2vec.similarity(new_tag, 'common')
            all_tags.append(new_tag)
            ignore_hyphen[new_tag] = tag
            ignore_hyphen[space_separated] = tag
            exact_matches_only.append(new_tag)
        except Exception as e:
            ignore_hyphen[space_separated] = tag
            pass

ignore_hyphen['digital']='e-commerce'
ignore_hyphen['online']= 'e-commerce'
ignore_hyphen['technology']= 'e-commerce'
ignore_hyphen['amazon']='e-commerce'
ignore_hyphen['flipkart']= 'e-commerce'
ignore_hyphen['internet']= 'e-commerce'
print(ignore_hyphen)


# In[ ]:


ignore_hyphen = dict()
print("all_tags: ", all_tags)
for tag in copy.copy(all_tags):
    if '-' in tag:
        print(tag)
        new_tag = re.sub('[^a-zA-Z0-9]', '', tag)
        space_separated = re.sub('[^a-zA-Z0-9]', ' ', tag)
        try:
            pre_trained_word2vec.similarity(new_tag, 'common')
            all_tags.append(new_tag)
            ignore_hyphen[new_tag] = tag
            ignore_hyphen[space_separated] = tag
            exact_matches_only.append(new_tag)
        except Exception as e:
            ignore_hyphen[space_separated] = tag
            pass
print(ignore_hyphen)


# In[ ]:


split_to_original = dict()
for category in copy.copy(list(set(all_categories_list))):
    if not category.isalpha():
        split_words = re.split('; |, |\*|\n|&| ',category)
        for i in split_words:
            if len(i)>0:
                if i in split_to_original:
                    #del split_to_original[i]
                    split_to_original[i].extend([category])
                else:
                    split_to_original[i]=[category]
                all_categories_list.append(i)
        all_categories_list.remove(category)


# In[ ]:


split_to_original = dict()
for category in copy.copy(list(set(all_tags))):
    if not category.isalpha():
        if '-' in category:
            split_words = re.split('-',category)
        else:
            split_words = re.split(' ',category)
        for i in split_words:
            if len(i)>0:
                if i in split_to_original:
                    #del split_to_original[i]
                    split_to_original[i].extend([category])
                else:
                    split_to_original[i]=[category]
                all_tags.append(i)
        all_tags.remove(category)


# In[ ]:


print(split_to_original)


# In[ ]:


from math import log

# Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
words = open("words-by-frequency.txt").read().split()
wordcost = dict((k, log((i+1)*log(len(words)))) for i,k in enumerate(words))
maxword = max(len(x) for x in words)

def infer_spaces(s):
    """Uses dynamic programming to infer the location of spaces in a string
    without spaces."""

    # Find the best match for the i first characters, assuming cost has
    # been built for the i-1 first characters.
    # Returns a pair (match_cost, match_length).
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i-maxword):i]))
        return min((c + wordcost.get(s[i-k-1:i], 9e999), k+1) for k,c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1,len(s)+1):
        c,k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i>0:
        c,k = best_match(i)
        assert c == cost[i]
        out.append(s[i-k:i])
        i -= k

    s= " ".join(reversed(out))
    return s.split()


# In[ ]:


def check_handle(x):
    try:
        return x.keywords.split('~')
    except:
        return ''
    
def isNaN(num):
    return num != num

df['keyword'] = df.apply(lambda x: [re.sub('[^a-zA-Z]' , '' , n.strip().lower())  for n in check_handle(x)], axis=1)
df['keyword'] = df.apply(lambda x: [a for n in x.keyword for a in infer_spaces(n)], axis=1)
df['keyword'] = df.apply(lambda x: [a for a in x.keyword if len(a)>2], axis=1)
df['all_categories'] = df.apply(lambda x: [re.sub('[^a-zA-Z]' , '' , n.strip().lower())  for n in x.all_categories.split('~')], axis=1)
df['all_categories'] = df.apply(lambda x: [a for n in x.all_categories for a in infer_spaces(n)], axis=1)
df['all_categories'] = df.apply(lambda x: [a for a in x.all_categories if len(a)>1], axis=1)
print(df.head())


# In[ ]:


tags = list(set(all_tags))

temp_tags = copy.copy(tags)
for term in temp_tags:
    try:
        pre_trained_word2vec.similarity(term, 'common')
    except Exception as e:
        try:
            tags.remove(term)
        except Excetion as e:
            print(e)
            pass
tags = sorted(list(set(tags)))
print(tags)


# In[ ]:


import copy
list_for_keyword_model = copy.copy(tags)  


conf = np.zeros(len(tags))
tag_to_idx= dict(zip(sorted(tags), range(len(tags))))
print(tag_to_idx)


cat_conf = np.zeros(len(top_categories))
cat_to_idx= dict(zip(sorted(top_categories), range(len(top_categories))))
print(cat_to_idx)

import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
stopwords.words('english')
stop_words = stopwords.words('english')
stop_words.append('podcast')
stop_words.append('logy')
stop_words.append('ing')


# In[ ]:


sexual_words = ['sexploitation', 'utube', 'barebacking', 'raunchy', 'topless', 'sextape', 'blowjob', 'porny', 'pornography', 'pornstars', 'xtube', 'sexcapades', 'porn', 'perv', 'upskirt', 'xxxchurch.com', 'pr0n', 'chatlines', 'nonpornographic', 'p0rn', 'nude', 'cyberporn', 'rentboy.com', 'cybersex', 'fleshbot', 'smut', 'homewrecking', 'pornos', 'pervy', 'nudies', 'pornstar', 'pornographers', 'perving', 'erotic', 'rentboy', 'nudity', 'sleazy', 'erotica', 'sex', 'kink.com', 'softcore', 'porno', 'pervs', 'hentai', 'skeevy', 'pornographic', 'pedo', 'shemale', 'youporn', 'xxx', 'pornographer', 'condomless', 'gangbang', 'nudie', 'whitehouse.com', 'beastiality', 'pedophilic', 'pedophile', 'redtube', 'paedophilic', 'lonelygirl']
sexual_words.extend(['sexploitation', 'utube', 'faggy', 'raunchy', 'tube.com', 'topless', 'sextape', 'seedy', 'porny', 'pornography', 'pornstars', 'trashy', 'titty', 'lezzie', 'splatterfest', 'xtube', 'fucks', 'porn', 'perv', 'upskirt', 'bdsm', 'pr0n', 'skanky', 'cammed', 'nudey', 'p0rn', 'boobie', 'cheezy', 'teensploitation', 'smut', 'homewrecking', 'pornos', 'bukkake', 'nudies', 'skeezy', 'pornstar', 'erotic', 'rentboy', 'trannie', 'cumshot', 'wank', 'sleazy', 'disneyland', 'erotica', 'sex', 'pimp', 'softcore', 'horny', 'skeevy', 'pedo', 'porno', 'hentai', 'pornographic', 'slutty', 'shemale', 'xxx', 'peepshow', 'skinemax', 'pornographer', 'risque', 'destricted', 'goatse', 'shemales', 'sleezy', 'gangbang', 'condomless', 'nudie', 'whitehouse.com', 'kinky', 'beastiality', 'coochie', 'schlock', 'flix', 'pedophilic', 'lesbo', 'redtube', 'pervy', 'sweded'])
sexual_words.extend(['romance', 'adult'])

def similarity_scores(word, tags):
    print(word)
    print(tags[0])
    for tag in tags:
        print(pre_trained_word2vec.similarity(word, tag))
        print(f"{word} - {tag}: {pre_trained_word2vec.similarity(word, tag)}")
        
def remove_outliers(l, keywords):
    for element in copy.copy(l):
        if element=='career':
            continue
        outlier_found = True
        for other_elements in copy.copy(l):
            try:
                keyword_score = keyword_model.similarity(element, other_elements)
            except:
                keyword_score = 1
            if element!= other_elements and (pre_trained_word2vec.similarity(element, other_elements) > 0.2 and keyword_score>0.5):
                outlier_found = False
                continue
                
        if outlier_found and not element in keywords:
            try:
                l.remove(element)
            except:
                pass
    return l           


# In[ ]:


import json
import requests

#http://172.26.211.21/api/createNewTag
url = 'http://172.26.211.21/api/createNewTag'

authors = list(set(df['author'].tolist()))
authors.append('-')
try:
    authors.remove('')
except:
    pass
print("Unique authors: ", len(authors))
print("Pure tags: ", len(pure_tags))
print("unique pure tags: ", len(set(pure_tags)))


all_tags_with_authors = pure_tags
#all_tags_with_authors.extend(top_categories)
all_tags_with_authors.extend(authors)
#all_tags_with_authors.extend(top_categories)

tag_to_id_mapping = dict()
count = 0

tag_count = 0 
tag_list = []
tags_acc =[]
for tag in all_tags_with_authors:
    pload = {"tag_name": tag, "tag_type": 2}
    tag_list.append(pload)
    tags_acc.append(tag)
    if len(tag_list)>=25:
        t = json.dumps(tag_list)
        if ((count+1)%50==0):
            time.sleep(60)
            
        update_data = {
            "data": tag_list
            }
            
        t = json.dumps(update_data)
        r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
        print("r.text: ", r.text)
        string_of_ids = (r.text[r.text.rindex('['):r.text.rindex("]")].strip())
        list_of_ids = string_of_ids[1:].split(',')
        list_of_ids = [int(a) for a in list_of_ids]
        print("list_of_ids :", len(list_of_ids))
        new_dict = dict(zip(tags_acc, list_of_ids))
        tag_to_id_mapping.update(new_dict)
        tag_list = []
        tags_acc = []
        count+=1


# In[ ]:


# for tag in pure_tags:
#     pload = {"tag_name": tag, "tag_type": 2}
#     t = json.dumps(pload)
#     if ((count+1)%50==0):
#         time.sleep(60)
#     r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#     print(r.text)
#     id = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
#     tag_to_id_mapping[tag]=id
#     count+=1

print("Tag list: ", tag_list)
if ((count+1)%50==0):
    time.sleep(60)

update_data = {
    "data": tag_list
    }
        
t = json.dumps(update_data)
r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
print(r.text)
string_of_ids = (r.text[r.text.rindex('['):r.text.rindex("]")].strip())
list_of_ids = string_of_ids[1:].split(',')
list_of_ids = [int(a) for a in list_of_ids]
print("list_of_ids :", list_of_ids)
new_dict = dict(zip(tags_acc, list_of_ids))
tag_to_id_mapping.update(new_dict)
count+=1


print(len(tag_to_id_mapping))


# In[ ]:


print(tag_to_id_mapping)


# In[ ]:


import json
import requests

url = 'http://172.26.211.21/api/createNewTag'
cat_to_id_mapping = dict()
# for cat in top_categories:
#     pload = {"cat_name": cat, "cat_type": 1}
#     t = json.dumps(pload)
#     if ((count+1)%50==0):
#         time.sleep(60)
#     r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#     id = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
#     cat_to_id_mapping[cat]=id
#     count+=1

cat_count = 0 
cat_list = []
cats_acc =[]


for cat in top_categories:
    pload = {"tag_name": cat, "tag_type": 1}
    cat_list.append(pload)
    cats_acc.append(cat)


t = json.dumps(cat_list)
if ((count+1)%50==0):
    time.sleep(60)

update_data = {
    "data": cat_list
    }
print(update_data)

t = json.dumps(update_data)
r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
print("r.text: ", r.text)
string_of_ids = (r.text[r.text.rindex('['):r.text.rindex("]")].strip())
list_of_ids = string_of_ids[1:].split(',')
list_of_ids = [int(a) for a in list_of_ids]
print("list_of_ids :", len(list_of_ids))
new_dict = dict(zip(cats_acc, list_of_ids))
cat_to_id_mapping.update(new_dict)
cat_list = []
cats_acc = []
count+=1
    
print(cat_to_id_mapping)


# In[ ]:


tag_to_id_mapping.update(cat_to_id_mapping)


# In[ ]:


def predict_tag(word, conf, no_of_keywords):
    cat = ''
    if word=='ironman':
        return conf, no_of_keywords
    

    if word=='cyber' and 'cyber' in tags:
        conf[tag_to_idx.get('cyber')] += no_of_keywords
        return conf, no_of_keywords
        
    if word=='security' and 'security' in tags:
        conf[tag_to_idx.get('security')] += no_of_keywords
        return conf, no_of_keywords 
        
    for cat in tags:
        threshold = 0.6
        value = 0
        multiplication_factor = 0.824 #Carefully designed
        
        #Hard-coding 1 category 
        if (cat=='workout' and word=='business') or (cat=='workout' and word=='marketing'):
            continue

        if (cat=='horror' and word in ['films', 'film', 'tv', 'review', 'reviews', 'movie', 'movies', 'kid', 'literary']):
            continue
            
        if (cat=='martial' and word=='arts'):
            continue
            
        if (cat=='mobility'):
            if (word in ['logistics', 'vehicles', 'vehicle']):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            else:
                continue

        if (cat=='sexuality'):
            if (word in ['amory', 'swinger', 'swingers', 'bdsm', 'poly', 'intimacy', 'swinging', 'bisexual', 'pride', 'sex', 'relationships', 'relationship', 'trans', 'tantra', 'porn', 'psychologist', 'couples', 'workers', 'ocd', 'sobriety', 'erotica', 'transgender', 'sexual', 'marriage', 'married']):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            else:
                continue



        if (word.endswith('s') or cat.endswith('s')) and (len(word)>3 and len(cat)>3) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat) or word == p.singular_noun(cat):
                if 'remix'==word:
                    print('remix identified')
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word) or cat == p.singular_noun(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
                if 'remix'==word:
                    print('remix identified')
            continue
            
        if (word.endswith('ing') or cat.endswith('ing')) and (len(word)>3 and len(cat)>3) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            continue
            
        if (word.endswith('tion') or cat.endswith('tion')) and (len(word)>4 and len(cat)>4) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            continue
            
#         if (word=='story' or word=='storytelling') and 'stories' in tags:
#             conf[tag_to_idx.get('stories')] += 1.0
            
        #hard-coding 2
        if 'technology' in tags and (word=='crypto' or word=='bitcoin' or word=='block' or word=='chain'):
            conf[tag_to_idx.get('technology')] += no_of_keywords
        
        #hard-coding 3
        if 'health' in tags and (word=='healthcare' or word=='bacteria' or word=='virus' or word=='organism'):
            conf[tag_to_idx.get('health')] += 1.0*no_of_keywords-0.001
        
        #hard-coding 4
        if 'science' in tags and (word=='bacteria' or word=='virus' or word=='organism'):
            conf[tag_to_idx.get('science')] += 0.5*no_of_keywords-0.001
        
        #hard-coding 5
        if 'fairytales' in tags and word=='science':
            conf[tag_to_idx.get('fairytales')] = -no_of_keywords
        
        #hard-coding 6
        if 'economics' in tags and word=='government':
            conf[tag_to_idx.get('economics')] = -0.05
            
        #hard-coding 7
        if cat=='betting':
            if (word=='betting' or word=='bet' or word=='gamble' or word=='gambling'):
                conf[tag_to_idx.get('betting')] += no_of_keywords
            else:
                continue
                
        if cat=='poetry':
            if (word in make_singular_plural_list(['poetry', 'poem', 'poetical', 'sonnet'])):
                conf[tag_to_idx.get('poetry')] += no_of_keywords
            else:
                continue
        
        try:
            #cat=keyword_model.most_similar_to_given(word, tags)
            value = keyword_model.similarity(word, cat)
            if value>threshold:
                #print(word, cat, value)
                if value>multiplication_factor:
                    conf[tag_to_idx.get(cat)] += value*(no_of_keywords-1)
                conf[tag_to_idx.get(cat)]+=value
            #print("Keyword model: ", keyword_model.similarity(word, cat))
        except Exception as e:
            try:
                #print("First exception: ", e)
                #cat=pre_trained_word2vec.most_similar_to_given(word, tags)
                value = pre_trained_word2vec.similarity(word, cat)
                if value>threshold:
                    if value>(multiplication_factor):
                        #print(word, cat, value)
                        conf[tag_to_idx.get(cat)] += value*(no_of_keywords-1)
                    conf[tag_to_idx.get(cat)]+=value
                #print("Pretrained: ", pre_trained_word2vec.similarity(word, cat))
            except Exception as e:
                if no_of_keywords>1:
                    no_of_keywords = no_of_keywords-1
                break
                    
        if cat == 'hollywood' and not word=='hollywood':
            if value>threshold:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value
        
        if cat == 'bollywood' and not word=='bollywood':
            if value>multiplication_factor:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value
        
        if cat in ('asian', 'disney', 'erotica', 'fairytales', 'thriller'):
            if value>multiplication_factor:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value

        
            #print("cat-word :", cat, word)
            value=1.0        
            if word in ('fairytale', 'fairytales', 'fairy', 'cinderella', 'cinderalla'):
                if 'fairytales' in tag_to_idx:
                    tag_under_consideration = 'fairytales'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords

            elif word in ('erotica', 'porn', 'bdsm', 'lesbian', 'softcore', 'nudes'):
                if 'erotica' in tag_to_idx:
                    tag_under_consideration = 'erotica'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords

            elif word in ('epcot', 'disneyland', 'disney', 'dw', 'walt'):
                if 'disney' in tag_to_idx:
                    tag_under_consideration = 'disney'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords

            elif word in ('asian', 'chinese', 'japanese', 'china', 'japan'):
                if 'asian' in tag_to_idx:
                    tag_under_consideration = 'asian'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            elif word in ('thriller', 'suspense', 'suspenseful'):
                if 'thriller' in tag_to_idx:
                    tag_under_consideration = 'thriller'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords

        
            else:
                for tag_ in ['erotica', 'fairytales']:
                    if not word=='fiction':
                        try:
                            value = pre_trained_word2vec.similarity(tag_, word)
                            if value>0.505 and tag_ in tag_to_idx:
                                conf[tag_to_idx.get(tag_)] += value*no_of_keywords
                            elif value>0.45 and value<=0.505 and tag_ in tag_to_idx:
                                conf[tag_to_idx.get(tag_)] += value
                            elif not word =='orlando' and cat=='disney':
                                value = pre_trained_word2vec.similarity('disney', word)
                                if value>0.5 and value<0.6 and 'disney' in tag_to_idx:
                                    conf[tag_to_idx.get('disney')] += value
                                elif value > 0.6 and 'disney' in tag_to_idx:
                                    conf[tag_to_idx.get('disney')] += value*no_of_keywords
                        except Exception as e:
                            if no_of_keywords>1:
                                no_of_keywords -=1
                            break

                            
    return conf, no_of_keywords


# In[ ]:


def predict_category(keywords, top_categories, cat_conf):
    keywords = list(set(keywords))
    no_of_keywords = len(keywords)
    valid_keywords =  []
    value = 0
    for word in keywords:
        if word=='self' or word=='ironman':
            no_of_keywords -=1
            continue
        
        for cat in top_categories:
            if (cat=='workout' and word=='business') or (cat=='workout' and word=='marketing'):                
                continue
            if (cat=='education' and word=='business'):
                cat_conf[cat_to_idx.get('education')]+=0.2
            if (cat=='education' and word=='marketing'):
                cat_conf[cat_to_idx.get('education')]+=0.2
#             if (cat=='stories' and word=='story'):
#                 cat_conf[cat_to_idx.get('stories')]+=0.2
                
            if (word=='story' or word=='storytelling') and ('stories'==cat or 'story'==cat):
                cat_conf[cat_to_idx.get(cat)] += 1

            if (cat=='music' and word=='poetry') or (cat=='music' and word=='shayari'):
                continue
            if 'entertainment'==cat and (word=='poetry' or word=='shayari'):
                conf[tag_to_idx.get('entertainment')] += 1.0*no_of_keywords-0.001

                               
            try:
                value = keyword_model.similarity(word, cat)
                valid_keywords.append(word)
                if ((cat=='workout' or cat=='astrology') and value>0.5) or (not (cat=='workout' or cat=='astrology') and value>0.2):
                    cat_conf[cat_to_idx.get(cat)]+=value
                elif cat==comedy or cat=='meditation':
                    print(mediation)
                    pass
            except Exception as e:
                try:
                    value = pre_trained_word2vec.similarity(word, cat)
                    valid_keywords.append(word)
                    if ((cat=='workout' or cat=='astrology') and value>0.5) or (cat=='comedy' and value>0.5) or (not (cat=='comedy' or cat=='workout' or cat=='astrology') and value>0.2):
                        cat_conf[cat_to_idx.get(cat)]+=value
                except Exception as e:
                    #print("Exception is: ", e)
                    if ('story' in word or 'stories' in word) and 'stories' in top_categories:
                        value = keyword_model.similarity('stories', 'stories')
                        cat_conf[cat_to_idx.get('stories')]+=value
                    if no_of_keywords>1:
                        no_of_keywords = no_of_keywords-1
                    break
    
    cat_conf_ = [value/no_of_keywords for value in cat_conf]
    cat_conf = np.array(cat_conf_)

    
    valid_keywords = list(set(valid_keywords))
    arr1 = np.sort(cat_conf)
    
    if 'comedy' in top_categories and arr1[-int(len(top_categories)/2)]<=cat_conf[cat_to_idx.get('comedy')]:
        try:
            if pre_trained_word2vec.similarity(pre_trained_word2vec.most_similar_to_given('comedy', valid_keywords), 'comedy')>0.5:
                cat_conf[cat_to_idx.get('comedy')]+=0.2
            else:
                cat_conf[cat_to_idx.get('comedy')]-=0.5
            #print(valid_keywords, pre_trained_word2vec.similarity(pre_trained_word2vec.most_similar_to_given('comedy', valid_keywords), 'comedy'))
        except:
            pass

    if 'comedy' in top_categories and 'stories' in top_categories and ('drama' in keywords or 'thriller' in keywords  or 'fiction' in keywords) and not 'comedy' in keywords:
        cat_conf[cat_to_idx.get('comedy')]=0.0
        cat_conf[cat_to_idx.get('stories')]+=0.2
        
    if 'kids' in top_categories and arr1[-int(len(top_categories)/2)]<=cat_conf[cat_to_idx.get('kids')] and len(intersection(sexual_words, keywords))>0:
        cat_conf[cat_to_idx.get('kids')]=0.0 
    
    return cat_conf


# In[ ]:


for term in tags:
    try:
        keyword_model.similar_by_word(term)
    except Exception as e:
        try:
            list_for_keyword_model.remove(term)
        except:
            pass
        
try:
    list_for_keyword_model.remove('fairytales')
except:
    pass


# In[ ]:


print(f'{(time.time()-start_time)/60:.2f} mins')
print(time.ctime())


# In[ ]:


mydb = mysql.connector.connect(
  host="172.26.115.140",
  user="RdonlYmilP",
  password="rd0nlymiLp140"
)

show_list = df.id.tolist()
show_list = [str(a) for a in show_list]
show_list = tuple(show_list)
crsr = mydb.cursor() 
query = f"select podcast_show_id, tag_id, tag_type from music_cms_logs.podcast_show_tags_logs where podcast_show_id in {show_list} and created_by>1 group by 1,2,3" 
crsr.execute(query)
#crsr.execute(query, show_list)

negative_df = pd.DataFrame(crsr)
if negative_df.shape[0]==0:
    negative_df = pd.DataFrame(pd.np.empty((0, 3))) 
negative_df.columns = ['podcast_show_id', 'tag_id', 'tag_type']
print(negative_df)
print(negative_df.shape)


# In[ ]:


stemmed_categories = [lemmatizer.lemmatize(cat) for cat in top_categories]
idx_to_tag = {v: k for k, v in tag_to_idx.items()}
idx_to_cat = {v: k for k, v in cat_to_idx.items()}


# In[ ]:



prediction = []
actual_tags = []
predicted_category = []
rejected_categories = []
dict_for_update = {}
list_for_update = []
len_list = 0
acc = 0
count = 0
updates = 1
skipped = 0


list_for_update = []
for i in range(df_not_predicted.shape[0]):
        show_id = -1
        try:
            show_id=df_not_predicted['id'].iloc[i]
            if isNaN(show_id):
                show_id = -1
        except Exception as e:
            print(e)
        print("show_id:", show_id)
            
        title = ''
        try:
            title=df_not_predicted['title'].iloc[i]
            if isNaN(title):
                title = -1
        except Exception as e:
            print(e)
            
        show_id = show_id.item()
        pred_cat = []
        predicted_tags = []  
        if df_not_predicted['feed_resource'].iloc[i]==1:
            manual_tags = df_not_predicted['manual_categories'].iloc[i]
            print("manual tags: ", manual_tags)
            manual_tags = re.sub('[^a-zA-Z,&-]' , ' ' , manual_tags )
            manual_tags = re.sub('[&]' , ',' , manual_tags )
            manual_tag = [cat.lower().strip() for cat in manual_tags.split(",") if cat.lower().strip() in pure_tags and not cat.lower().strip() in pred_cat]
            predicted_tags.extend(manual_tag)
            print("manual tags: ", manual_tag)

          
        #predicted_tags = list(set(predicted_tags))
            
        #if not (author=='-' or author in predicted_tags):
        #    predicted_tags.append(author)
            
        #print("Input words: ", original_keywords)
        
        #print("Predicted tags: ", predicted_tags)
        #pred_cat = list(set(pred_cat))
        
        #print("Predicted categories: ", pred_cat)

        #exclusion = negative_df.loc[(negative_df['podcast_show_id']==show_id) & (negative_df['tag_type']==2), "tag_id"]
        #exclusion = exclusion.tolist()
        #x = [tag_to_id_mapping.get(tag) for tag in predicted_tags]
        x = []
        for tag in predicted_tags:
            if(tag in tag_to_id_mapping):
                x.append(tag_to_id_mapping.get(tag))
        #x = [a for a in x if not a in exclusion]
        
        predicted_tag_ids  = ','.join(str(e) for e in x)
        print("IDs as string: ", predicted_tag_ids)
        
        if df_not_predicted['feed_resource'].iloc[i]==1:
            manual_categories = df_not_predicted['manual_categories'].iloc[i]
            manual_categories = re.sub('[^a-zA-Z,&-]' , ' ' , manual_categories )
            manual_categories = re.sub('[&]' , ',' , manual_categories )
            manual_cat = [cat.lower().strip() for cat in manual_categories.split(",") if cat.lower().strip() in top_categories]
            pred_cat.extend(manual_cat)


        pred_cat = list(set(pred_cat))
        
        #exclusion = negative_df.loc[(negative_df['podcast_show_id']==show_id) & (negative_df['tag_type']==1), "tag_id"]
        #exclusion = exclusion.tolist()
        #print("negative tags:", exclusion)
        
        #print("Predicted categories: ", pred_cat)
        #x = [cat_to_id_mapping.get(cat) for cat in pred_cat]
        #x = [a for a in x if not a in exclusion]
        x = []
        for cat in pred_cat:
            if(cat in cat_to_id_mapping):
                x.append(cat_to_id_mapping.get(cat))
        pred_cat_ids  = ','.join(str(e) for e in x)
        #print("After removing negative tags ", pred_cat_ids)
        
#         url = 'http://172.26.211.21/api/updateTagsInShow'
#         pload = {
#             "tags_id":predicted_tag_ids,
#             "show_id":show_id,
#             "tag_type":2,
#             "is_override":1
#             }

#         t = json.dumps(pload)
#         r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#         print("tag: ", r.text)
        
#         url = 'http://172.26.211.21/api/updateTagsInShow'
#         pload = {
#             "tags_id":pred_cat_ids,
#             "show_id":show_id,
#             "tag_type":1,
#             "is_override":1
#             }
        
#         t = json.dumps(pload)
#         r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#         print("Category: ", r.text)
        
        tags_update = {
        "tags_id":predicted_tag_ids,
        "show_id":show_id,
        "tag_type":2,
        "is_override":1
        }
        
        category_update = {
        "tags_id":pred_cat_ids,
        "show_id":show_id,
        "tag_type":1,
        "is_override":1
        }
        
        list_for_update.extend([tags_update, category_update])
        len_list +=2
        
        
        if len_list%14==0:
            print("length list_for_update :", len(list_for_update))
            #url = 'https://cmsdev.gaana.com/api/bulkupdateTagsInShow'
            url = 'http://172.26.211.21/api/bulkupdateTagsInShow'
            update_data = {
            "data": list_for_update
            }
            
            t = json.dumps(update_data)
            
            if (updates%15==0):
                time.sleep(60)
                
            updates+=1
            print("Update number: ", updates)
            r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
            print("Update: ", r.text)
            list_for_update = []
            len_list= 0 
            print("length list_for_update :", len(list_for_update))

        fields = [show_id, title, predicted_tags, pred_cat]
        with open(filename1,'a+') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
            
        count+=1
        print("Count: ", count)
        
url = 'http://172.26.211.21/api/bulkupdateTagsInShow'
update_data = {
"data": list_for_update
}

t = json.dumps(update_data)

if (updates%15==0):
    time.sleep(60)

updates+=1
print("Update number: ", updates)
r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
print("Update: ", r.text)
list_for_update = []
len_list= 0 
print("length list_for_update :", len(list_for_update))




prediction = []
actual_tags = []
predicted_category = []
rejected_categories = []
dict_for_update = {}
list_for_update = []
len_list = 0
acc = 0
count = 0
updates = 1
skipped = 0
for i in range(df.shape[0]):
    #try:
        conf = np.zeros(len(tags))
        cat_conf = np.zeros(len(top_categories))
        pred_cat=[]
        keywords = []
        predicted_tags = []
        try:
            keywords = list(df['keyword'].iloc[i])
            
        except Exception as e:
            print(e)
            keywords = []
            
        categories_for_this = df['all_categories'].iloc[i]
        keywords.extend(categories_for_this)
        keywords = list(set(keywords))
        keywords = [key for key in keywords if not key in stop_words and len(key)>1]
        original_keywords = copy.copy(keywords)
        no_of_keywords = len(keywords)
        
        author = '-'
        try:
            author=df['author'].iloc[i]
            if isNaN(author) or len(author)==0:
                author = '-'
        except Exception as e:
            print(e)
            
        show_id = -1
        try:
            show_id=df['id'].iloc[i]
            if isNaN(show_id):
                show_id = -1
        except Exception as e:
            print(e)
        print("show_id:", show_id)
            
        title = ''
        try:
            title=df['title'].iloc[i]
            if isNaN(title):
                title = -1
        except Exception as e:
            print(e)
            
        show_id = show_id.item()
#         tag = author
#         url = 'http://172.26.211.21/api/createNewTag'
#         pload = {"tag_name": tag, "tag_type": 2}
#         t = json.dumps(pload)
#         if (count%50==0):
#             time.sleep(60)

#         r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#         id_no = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
#         tag_to_id_mapping[tag]=id_no
#         count+=1
        
        
        if no_of_keywords <=2:
            predicted_tags = [a for a in keywords if a in tags]
            pred_cat = [a for a in predicted_tags if a in top_categories]
            if len(pred_cat):
                for a in pred_cat:
                    predicted_tags.remove(a)
                    
                
            if not len(pred_cat):
                try:
                    cat_conf = np.zeros(len(top_categories))
                    cat_conf = predict_category(keywords, top_categories, cat_conf)
                    all_zeros = (cat_conf <= 0.0).all()
                    if all_zeros:
                        pred_cat=[]
                    else:
                        pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
                except:
                    pred_cat = ['-']
                    
            rejected = []
                
            if len(pred_cat)==1 and pred_cat[0]=='workout':
                cat_conf =  predict_category(keywords, top_categories, cat_conf)
                pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
                
            

#             prediction.append(predicted_tags)
#             predicted_category.append(pred_cat)
#             rejected_categories.append(rejected)
            #continue
    
        
            
        else:
            for each_word in keywords:
                conf, no_of_keywords = predict_tag(each_word, conf, no_of_keywords)

            scores = [(score/no_of_keywords) for score in conf]
            scores = np.array(scores)

            max_idx = np.where(scores>0.5, 1, 0)
            
            predicted_tags =  [idx_to_tag.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
            print("predicted soon after prediction: ", predicted_tags)
            
            predicted_tags = remove_outliers(predicted_tags, make_singular_plural_list(keywords)) #psychedelic is removed here because it is an outlier
            print("predicted after removal of outliers: ", predicted_tags)
            
        to_be_removed = []   
        if not 'security' in keywords:
            try:
                predicted_tags.remove('security')
            except:
                pass
           
        
        for tag in copy.copy(predicted_tags):
            replaced = False
            if tag in split_to_original:
                original_list = split_to_original[tag]
                for each_word in original_list:
                    x = (re.sub('[^a-zA-Z0-9]', ' ', each_word)).split()
                    x.remove(tag)
                    if len(x)>0 and x[0] in predicted_tags:
                        predicted_tags.append(each_word)
                        replaced = True
                            
                if len(original_list)>0 and not (tag in top_categories or tag in pure_tags):
                    to_be_removed.append(tag)
                
        predicted_tags = [tag for tag in predicted_tags if not tag in to_be_removed]
        
        predicted_tags = [(re.sub('[-]', ' ', a)) for a in predicted_tags] 

        #keywords = [(re.sub('[-]', ' ', a)) for a in keywords] 
        #pred_cat =[]  #If any tag is also a category, move that tag into the category list. 
        for tag in copy.copy(predicted_tags):
            if tag in top_categories:
                pred_cat.append(tag)
                predicted_tags.remove(tag)
        
        
        predicted_tags = list(set(predicted_tags))
        if 'kids' in predicted_tags and len(intersection(sexual_words, keywords))>0:
            predicted_tags.remove('kids')
            
        rejected = []
        
        if len(copy.copy(pred_cat))>=3 :
            cat_conf = np.zeros(len(top_categories))
            if 'astrology' in pred_cat and 'astrology' in keywords:
                cat_conf[cat_to_idx.get('astrology')]+=0.5
            if len(keywords)>0:
                cat_conf = predict_category(keywords, top_categories, cat_conf)
            try:
                cat_conf[cat_to_idx.get('workout')] = (cat_conf[cat_to_idx.get('workout')]>0.95).astype(int)
            except:
                print(e)
                pass
            
            rejected = copy.copy(pred_cat)
            positions_of_prediction = [cat_to_idx.get(a) for a in rejected]
            
            for k in range(len(cat_conf)):
                if k not in positions_of_prediction:
                    cat_conf[k]=0
            
            arr1 = np.sort(cat_conf)
            if arr1[-2]/arr1[-1]>0.9:
                threshold_position=2
            else:
                threshold_position=1
            threshold_position=2 #The number of tags you want to keep even after pruning
            max_idx = np.where(cat_conf>=arr1[-threshold_position], 1, 0)
            pred_cat =  [idx_to_cat.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
            for t in pred_cat:
                rejected.remove(t)
                       
        for each_word in rejected:
            if each_word in all_tags and each_word in keywords:
                predicted_tags.insert(0,each_word)
        
        #print("predicted_tags before removal: ", predicted_tags)
        redundant_tags = intersection(predicted_tags, exact_matches_only)

        if len(redundant_tags):
            tags_to_be_kept = intersection(redundant_tags, make_singular_plural_list(keywords))
            tags_to_be_removed = [a for a in redundant_tags if not a in tags_to_be_kept and not ' ' in a]
            if len(tags_to_be_removed):
                for a in tags_to_be_removed:
                    predicted_tags.remove(a)        
        #print("predicted_tags after removal: ", predicted_tags)            
        if len(copy.copy(pred_cat))==0:
            cat_conf = np.zeros(len(top_categories))
            cat_conf = predict_category(keywords, top_categories, cat_conf)
            arr1 = np.sort(cat_conf)
            threshold=0
            if arr1[-2]/arr1[-1]>0.9:
                threshold = arr1[-2]
            else:
                threshold = arr1[-1]
                
            if threshold<=0.0:
                pred_cat = []
            else:
                max_idx = np.where(cat_conf>=threshold, 1, 0)
                pred_cat =  [idx_to_cat.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
                #predicted_cat = remove_outliers(predicted_cat, keywords)
        
        if len(pred_cat)==1 and pred_cat[0]=='workout':
            cat_conf =  predict_category(keywords, top_categories, cat_conf)
            pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
            
        if len(keywords)==1 and keywords[0]=='miscellaneous':
                pred_cat = []

        
        pred_cat = list(set(pred_cat))
                
        if len(pred_cat) == 2 and 'health' in  pred_cat and 'entertainment' in pred_cat:
            pred_cat = ['entertainment']
            
        if len(pred_cat)<2:
            if len(intersection(motivation, predicted_tags))>0:
                pred_cat.append('motivation')
            elif len(intersection(meditation, predicted_tags))>0:
                pred_cat.append('meditation')
        
        for tag in copy.copy(predicted_tags):
            if tag in ignore_hyphen:
                if (not 'commerce' in ignore_hyphen[tag]) or ('commerce') in keywords:
                    print(ignore_hyphen[tag])
                    predicted_tags.append(ignore_hyphen[tag])
                    print(predicted_tags)
                    if not tag in pure_tags:
                        predicted_tags.remove(tag)
        
        predicted_tags = match_with_keywords(predicted_tags, keywords)                
        
        if 'fiction' in keywords and 'fiction' in all_tags:
            predicted_tags.insert(0, 'fiction')
            
        if 'cryptocurrency' in all_tags and ('crypto' in keywords and 'currency' in keywords):
            predicted_tags.insert(0, 'cryptocurrency')
            
        if 'blockchain' in all_tags and ('block' in keywords and 'chain' in keywords):
            predicted_tags.insert(0, 'blockchain')
            
        if 'bitcoin' in all_tags and (('bit' in keywords and 'coin' in keywords) or ('bitcoin' in keywords)):
            predicted_tags.insert(0, 'bitcoin')
            
        if 'cyber' in keywords and 'security' in keywords and 'hacking' in all_tags:
            predicted_tags.insert(0, 'hacking')
            
        if ('dj' in keywords or 'djs' in keywords) and 'dj' in all_tags:
            predicted_tags.insert(0, 'dj')
            
        if ('dj' in keywords or 'djs' in keywords) and 'djs' in all_tags:
            predicted_tags.insert(0, 'djs')
            
        if 'edm' in keywords and 'edm' in all_tags:
            predicted_tags.insert(0, 'edm')

            
        
        if df['feed_resource'].iloc[i]==1:
            manual_tags = df['manual_categories'].iloc[i]
            print("manual tags: ", manual_tags)
            manual_tags = re.sub('[^a-zA-Z,&-]' , ' ' , manual_tags )
            manual_tags = re.sub('[&]' , ',' , manual_tags )
            manual_tag = [cat.lower().strip() for cat in manual_tags.split(",") if cat.lower().strip() in pure_tags and not cat.lower().strip() in pred_cat]
            predicted_tags.extend(manual_tag)
            print("manual tags: ", manual_tag)

            
        predicted_tags = list(set(predicted_tags))
            
        if not (author=='-' or author in predicted_tags):
            predicted_tags.append(author)
            
        print("Input words: ", original_keywords)
        
        print("Predicted tags: ", predicted_tags)
        pred_cat = list(set(pred_cat))
        print("Predicted categories: ", pred_cat)

        exclusion = negative_df.loc[(negative_df['podcast_show_id']==show_id) & (negative_df['tag_type']==2), "tag_id"]
        exclusion = exclusion.tolist()
        x = [tag_to_id_mapping.get(tag) for tag in predicted_tags]
        x = [a for a in x if not a in exclusion]
        
        predicted_tag_ids  = ','.join(str(e) for e in x)
        print("IDs as string: ", predicted_tag_ids)
        
        if df['feed_resource'].iloc[i]==1:
            manual_categories = df['manual_categories'].iloc[i]
            manual_categories = re.sub('[^a-zA-Z,&-]' , ' ' , manual_categories )
            manual_categories = re.sub('[&]' , ',' , manual_categories )
            manual_cat = [cat.lower().strip() for cat in manual_categories.split(",") if cat.lower().strip() in top_categories]
            pred_cat.extend(manual_cat)

        if ('music' in pred_cat) and (('poetry' in predicted_tags or 'poetry' in original_keywords) or ('shayari' in predicted_tags or 'shayari' in original_keywords)) and not ('music' in predicted_tags or 'music' in original_keywords or 'songs' in predicted_tags or 'songs' in original_keywords):
            try:
                pred_cat.remove('music')
            except:
                pass
            pred_cat.append('entertainment')


        pred_cat = list(set(pred_cat))
        
        exclusion = negative_df.loc[(negative_df['podcast_show_id']==show_id) & (negative_df['tag_type']==1), "tag_id"]
        exclusion = exclusion.tolist()
        
        print("Predicted categories: ", pred_cat)
        x = [cat_to_id_mapping.get(cat) for cat in pred_cat]
        x = [a for a in x if not a in exclusion]
        pred_cat_ids  = ','.join(str(e) for e in x)
        
#         url = 'http://172.26.211.21/api/updateTagsInShow'
#         pload = {
#             "tags_id":predicted_tag_ids,
#             "show_id":show_id,
#             "tag_type":2,
#             "is_override":1
#             }

#         t = json.dumps(pload)
#         r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#         print("tag: ", r.text)
        
#         url = 'http://172.26.211.21/api/updateTagsInShow'
#         pload = {
#             "tags_id":pred_cat_ids,
#             "show_id":show_id,
#             "tag_type":1,
#             "is_override":1
#             }
        
#         t = json.dumps(pload)
#         r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
#         print("Category: ", r.text)
        
        tags_update = {
        "tags_id":predicted_tag_ids,
        "show_id":show_id,
        "tag_type":2,
        "is_override":1
        }
        
        category_update = {
        "tags_id":pred_cat_ids,
        "show_id":show_id,
        "tag_type":1,
        "is_override":1
        }
        
        list_for_update.extend([tags_update, category_update])
        len_list +=2
        
        
        if len_list%14==0:
            print("length list_for_update :", len(list_for_update))
            #url = 'https://cmsdev.gaana.com/api/bulkupdateTagsInShow'
            url = 'http://172.26.211.21/api/bulkupdateTagsInShow'
            update_data = {
            "data": list_for_update
            }
            
            t = json.dumps(update_data)
            
            if (updates%15==0):
                time.sleep(60)
                
            updates+=1
            print("Update number: ", updates)
            r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
            print("Update: ", r.text)
            list_for_update = []
            len_list= 0 
            print("length list_for_update :", len(list_for_update))
            
            
        
        fields = [show_id, title, original_keywords, predicted_tags, pred_cat]
        with open(filename,'a+') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
            
        count+=1
        print("Count: ", count)
#     except Exception as e:
#         print("Second exception", e)
#         print(show_id)

url = 'http://172.26.211.21/api/bulkupdateTagsInShow'
update_data = {
"data": list_for_update
}

t = json.dumps(update_data)

time.sleep(60)

updates+=1
print("Update number: ", updates)
r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
print("Update: ", r.text)
list_for_update = []
len_list= 0 
print("length list_for_update :", len(list_for_update))

# In[ ]:


print(len(prediction))
print(df.shape)

print(f'{(time.time()-start_time)/60:.2f} mins')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





