# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* It contains the codes required to run the autotagging script for Podcasts.
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* cd to the location where you want to download/clone this repository
* git clone https://satyamsingh1@bitbucket.org/satyamsingh1/podcasttagging.git
* Configuration: Python 3, 
* Dependencies
* To download: Google word2vec, nltk, gensim, mysql
* Access to some DBs

### Download guidelines ###

* wget -c "https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz"
* pip3 install nltk
* pip3 install gensim

### Which file to run? ###

* PodcastTaggingLive.py

### Who do I talk to? ###

* Repo owner or admin: satyam.singh1@gaana.com
* Other community or team contact (in case of unavailability: search.reco@gaana.com)