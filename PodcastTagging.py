#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import random
from gensim.models import Word2Vec 
import copy

import warnings;
warnings.filterwarnings('ignore')

import re
import csv

from nltk.stem import WordNetLemmatizer
import nltk
nltk.download('wordnet')

import time
import datetime
start_time = time.time()
print(time.ctime())


# In[2]:


record = ['show_id', 'title', 'keywords+category', 'predicted_tags', 'tag_ids', 'predicted_categories', 'category_ids', 'rejected']
with open('Podcasts.csv','w+') as f:
    writer = csv.writer(f)
    writer.writerow(record)


# In[3]:


import mysql.connector

# mydb = mysql.connector.connect(
#   host="172.26.111.237",
#   user="StgPlayApp",
#   password="AppHkwH2cBf"
# )

mydb = mysql.connector.connect(
  host="172.26.98.167",
  user="RdnLYctxMus",
  password="RlUsRMzT920"
)

# today_date_string = today_date
# print(today_date_string)
# prev_date_string = prev_date.strftime('%Y-%m-%d %H:%M:%S')
    
starting_day = datetime.date.today()-datetime.timedelta(1)
starting_day = starting_day.strftime('%Y-%m-%d %H:%M:%S')
print(starting_day)
end_day = datetime.date.today()-datetime.timedelta(0)
end_day = end_day.strftime('%Y-%m-%d %H:%M:%S')
print(end_day)

crsr = mydb.cursor() 
query = f"SELECT id, title, keywords, author, summary, all_categories FROM music.tm_podcasts_show where modified_on between ('{starting_day}') and ('{end_day}') and status='1'"

crsr.execute(query)
#crsr.execute(query, (starting_day, end_day) )

df = pd.DataFrame(crsr)
print(df.head())
print(df.shape)


# In[4]:


def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3 


# In[5]:


df.columns= ['id', 'title', 'keywords', 'author', 'summary', 'all_categories']


# In[6]:


df['all_categories'].replace('', np.nan, inplace=True)
df.dropna(subset=['all_categories'], inplace=True)
#Both_DFs.dropna(subset=['keywords'], inplace=True)
print(df.shape)


# In[7]:


tags_categories = pd.read_csv('Category-wise Tags - TagsVipul.csv')
categories = tags_categories.columns.tolist()
print(categories)
all_tags = []

lemmatizer = WordNetLemmatizer() 


# In[8]:


for column in categories:
    all_tags.extend(tags_categories[f'{column}'].tolist())

pure_tags = copy.copy(all_tags)
pure_tags = [tag.lower() for tag in pure_tags if str(tag) != 'nan']
all_tags.extend(categories)    
all_tags = [tag.lower() for tag in all_tags if str(tag) != 'nan']
print(all_tags)

top_categories = categories
try:
    top_categories.remove('Category')
except Exception as e:
    try:
        del top_categories[0]
    except Eception as e:
        pass

tag_cat = dict()
for category in top_categories:
    tags = tags_categories[f'{category}']
    tags = [tag.lower() for tag in tags if str(tag) != 'nan']
    tag_cat.update(zip(tags, [category.lower()]*len(tags)))
    
top_categories = [category.lower() for category in top_categories]
print(top_categories)


# In[9]:


exact_matches_only = ['ayurveda', 'buddhism', 'hundusim', 'islam', 'christianity', 'British','indian', 'mythology', 'American', 'Cricket','Baseball','Basketball','Tennis','Football','Soccer', 'Hockey','Kabaddi', 'Black','Hispanic','Asian','European','African','Chinese','SouthAmerican', 'Russian', 'Japanese', 'English', 'Hindi','Spanish', 'French', 'Chinese', 'Latin', 'fiction', 'non-fiction']
exact_matches_only = [a.lower() for a in exact_matches_only]


# In[10]:


motivation = ['self improvement', 'self help']

meditation = ['mindfulness, meditation']

import gensim
pre_trained_word2vec = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)
keyword_model = gensim.models.KeyedVectors.load_word2vec_format('keywords_after_splitting_word2vec.bin', binary=True)

print(f"{(time.time()-start_time)/60:.2f} mins")


# In[11]:


all_categories = df['all_categories'].tolist()
all_categories_list = []
for category_list in all_categories:
    one_word_tags = []
    tags = (category_list.split(','))
    for tag in tags:
        if not tag.isalpha():
            one_word_tags.extend(re.sub('[^a-zA-Z]', ' ', tag).split())
        else:
            one_word_tags.append(tag)
    all_categories_list.extend(one_word_tags)
    
all_categories_list = [ a.lower().strip() for a in all_categories_list ]
all_categories_list = sorted(list((set(all_categories_list))))


# In[12]:


ignore_hyphen = dict()
print(all_tags)
for tag in copy.copy(all_tags):
    if '-' in tag:
        print(tag)
        new_tag = re.sub('[^a-zA-Z0-9]', '', tag)
        space_separated = re.sub('[^a-zA-Z0-9]', ' ', tag)
        try:
            pre_trained_word2vec.similarity(new_tag, 'common')
            all_tags.append(new_tag)
            ignore_hyphen[new_tag] = tag
            ignore_hyphen[space_separated] = tag
            exact_matches_only.append(new_tag)
        except Exception as e:
            ignore_hyphen[space_separated] = tag
            pass
print(ignore_hyphen)


# In[13]:


split_to_original = dict()
for category in copy.copy(list(set(all_categories_list))):
    if not category.isalpha():
        split_words = re.split('; |, |\*|\n|&| ',category)
        for i in split_words:
            if len(i)>0:
                if i in split_to_original:
                    #del split_to_original[i]
                    split_to_original[i].extend([category])
                else:
                    split_to_original[i]=[category]
                all_categories_list.append(i)
        all_categories_list.remove(category)


# In[14]:


split_to_original = dict()
for category in copy.copy(list(set(all_tags))):
    if not category.isalpha():
        if '-' in category:
            split_words = re.split('-',category)
        else:
            split_words = re.split(' ',category)
        for i in split_words:
            if len(i)>0:
                if i in split_to_original:
                    #del split_to_original[i]
                    split_to_original[i].extend([category])
                else:
                    split_to_original[i]=[category]
                all_tags.append(i)
        all_tags.remove(category)


# In[15]:


print(split_to_original)


# In[16]:


from math import log

# Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
words = open("words-by-frequency.txt").read().split()
wordcost = dict((k, log((i+1)*log(len(words)))) for i,k in enumerate(words))
maxword = max(len(x) for x in words)

def infer_spaces(s):
    """Uses dynamic programming to infer the location of spaces in a string
    without spaces."""

    # Find the best match for the i first characters, assuming cost has
    # been built for the i-1 first characters.
    # Returns a pair (match_cost, match_length).
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i-maxword):i]))
        return min((c + wordcost.get(s[i-k-1:i], 9e999), k+1) for k,c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1,len(s)+1):
        c,k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i>0:
        c,k = best_match(i)
        assert c == cost[i]
        out.append(s[i-k:i])
        i -= k

    s= " ".join(reversed(out))
    return s.split()


# In[17]:


def check_handle(x):
    try:
        return x.keywords.split('~')
    except:
        return ''
    
def isNaN(num):
    return num != num
#isNaN(df['Author'].iloc[48])


# In[18]:


df['keyword'] = df.apply(lambda x: [re.sub('[^a-zA-Z]' , '' , n.strip().lower())  for n in check_handle(x)], axis=1)
df['keyword'] = df.apply(lambda x: [a for n in x.keyword for a in infer_spaces(n)], axis=1)
df['keyword'] = df.apply(lambda x: [a for a in x.keyword if len(a)>2], axis=1)
df['all_categories'] = df.apply(lambda x: [re.sub('[^a-zA-Z]' , '' , n.strip().lower())  for n in x.all_categories.split('~')], axis=1)
df['all_categories'] = df.apply(lambda x: [a for n in x.all_categories for a in infer_spaces(n)], axis=1)
df['all_categories'] = df.apply(lambda x: [a for a in x.all_categories if len(a)>1], axis=1)
print(df.head())


# In[ ]:





# In[ ]:





# In[19]:


tags = list(set(all_tags))


# In[20]:


temp_tags = copy.copy(tags)
for term in temp_tags:
    try:
        pre_trained_word2vec.similarity(term, 'common')
    except Exception as e:
        try:
            tags.remove(term)
        except Excetion as e:
            print(e)
            pass


# In[21]:


tags = sorted(list(set(tags)))
print(tags)


# In[22]:


import copy
list_for_keyword_model = copy.copy(tags)  


# In[23]:


conf = np.zeros(len(tags))
tag_to_idx= dict(zip(sorted(tags), range(len(tags))))
print(tag_to_idx)


cat_conf = np.zeros(len(top_categories))
cat_to_idx= dict(zip(sorted(top_categories), range(len(top_categories))))
print(cat_to_idx)


# In[24]:


import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
stopwords.words('english')
stop_words = stopwords.words('english')
stop_words.append('podcast')
stop_words.append('logy')
stop_words.append('ing')


# In[25]:


sexual_words = ['sexploitation', 'utube', 'barebacking', 'raunchy', 'topless', 'sextape', 'blowjob', 'porny', 'pornography', 'pornstars', 'xtube', 'sexcapades', 'porn', 'perv', 'upskirt', 'xxxchurch.com', 'pr0n', 'chatlines', 'nonpornographic', 'p0rn', 'nude', 'cyberporn', 'rentboy.com', 'cybersex', 'fleshbot', 'smut', 'homewrecking', 'pornos', 'pervy', 'nudies', 'pornstar', 'pornographers', 'perving', 'erotic', 'rentboy', 'nudity', 'sleazy', 'erotica', 'sex', 'kink.com', 'softcore', 'porno', 'pervs', 'hentai', 'skeevy', 'pornographic', 'pedo', 'shemale', 'youporn', 'xxx', 'pornographer', 'condomless', 'gangbang', 'nudie', 'whitehouse.com', 'beastiality', 'pedophilic', 'pedophile', 'redtube', 'paedophilic', 'lonelygirl']
sexual_words.extend(['sexploitation', 'utube', 'faggy', 'raunchy', 'tube.com', 'topless', 'sextape', 'seedy', 'porny', 'pornography', 'pornstars', 'trashy', 'titty', 'lezzie', 'splatterfest', 'xtube', 'fucks', 'porn', 'perv', 'upskirt', 'bdsm', 'pr0n', 'skanky', 'cammed', 'nudey', 'p0rn', 'boobie', 'cheezy', 'teensploitation', 'smut', 'homewrecking', 'pornos', 'bukkake', 'nudies', 'skeezy', 'pornstar', 'erotic', 'rentboy', 'trannie', 'cumshot', 'wank', 'sleazy', 'disneyland', 'erotica', 'sex', 'pimp', 'softcore', 'horny', 'skeevy', 'pedo', 'porno', 'hentai', 'pornographic', 'slutty', 'shemale', 'xxx', 'peepshow', 'skinemax', 'pornographer', 'risque', 'destricted', 'goatse', 'shemales', 'sleezy', 'gangbang', 'condomless', 'nudie', 'whitehouse.com', 'kinky', 'beastiality', 'coochie', 'schlock', 'flix', 'pedophilic', 'lesbo', 'redtube', 'pervy', 'sweded'])
sexual_words.extend(['romance', 'adult'])


# In[26]:


def similarity_scores(word, tags):
    print(word)
    print(tags[0])
    for tag in tags:
        print(pre_trained_word2vec.similarity(word, tag))
        print(f"{word} - {tag}: {pre_trained_word2vec.similarity(word, tag)}")
        
    #return null


# In[27]:


def remove_outliers(l, keywords):
    for element in copy.copy(l):
        if element=='career':
            continue
        outlier_found = True
        for other_elements in copy.copy(l):
            try:
                keyword_score = keyword_model.similarity(element, other_elements)
            except:
                keyword_score = 1
            if element!= other_elements and (pre_trained_word2vec.similarity(element, other_elements) > 0.2 and keyword_score>0.5):
                outlier_found = False
                continue
                
        if outlier_found and not element in keywords:
            try:
                l.remove(element)
            except:
                pass
    return l           


# In[28]:


import json
import requests

url = 'https://cmsdev.gaana.com/api/createNewTag'

tag_to_id_mapping = dict()
for tag in pure_tags:
    pload = {"tag_name": tag, "tag_type": 2}
    t = json.dumps(pload)
    r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
    id = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
    tag_to_id_mapping[tag]=id
    
print(tag_to_id_mapping)


# In[29]:


import json
import requests

url = 'https://cmsdev.gaana.com/api/createNewTag'
cat_to_id_mapping = dict()
for cat in top_categories:
    pload = {"tag_name": cat, "tag_type": 1}
    t = json.dumps(pload)
    r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
    id = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
    cat_to_id_mapping[cat]=id
    
print(cat_to_id_mapping)


# In[30]:


def predict_tag(word, conf, no_of_keywords):
    cat = ''
    for cat in tags:
        threshold = 0.6
        value = 0
        multiplication_factor = 0.82 #Carefully designed
        
        #Hard-coding 1 category 
        if (cat=='workout' and word=='business') or (cat=='workout' and word=='marketing'):
            continue

        if (word.endswith('s') or cat.endswith('s')) and (len(word)>3 and len(cat)>3) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            continue
            
        if (word.endswith('ing') or cat.endswith('ing')) and (len(word)>3 and len(cat)>3) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            continue
            
        if (word.endswith('tion') or cat.endswith('tion')) and (len(word)>4 and len(cat)>4) and word[:4] == cat[:4] and not word==cat:
            if word==lemmatizer.lemmatize(cat):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            elif cat==lemmatizer.lemmatize(word):
                conf[tag_to_idx.get(cat)] += no_of_keywords
            continue

        #hard-coding 2
        if 'technology' in tags and (word=='crypto' or word=='bitcoin' or word=='block' or word=='chain'):
            conf[tag_to_idx.get('technology')] += no_of_keywords
        
        #hard-coding 3
        if 'health' in tags and (word=='healthcare' or word=='bacteria' or word=='virus' or word=='organism'):
            conf[tag_to_idx.get('health')] += 1.0*no_of_keywords-0.001
        
        #hard-coding 4
        if 'science' in tags and (word=='bacteria' or word=='virus' or word=='organism'):
            conf[tag_to_idx.get('science')] += 0.5*no_of_keywords-0.001
        
        #hard-coding 5
        if 'fairytales' in tags and word=='science':
            conf[tag_to_idx.get('fairytales')] = -no_of_keywords
        
        try:
            #cat=keyword_model.most_similar_to_given(word, tags)
            value = keyword_model.similarity(word, cat)
            if value>threshold:
                #print(word, cat, value)
                if value>multiplication_factor:
                    conf[tag_to_idx.get(cat)] += value*(no_of_keywords-1)
                conf[tag_to_idx.get(cat)]+=value
            #print("Keyword model: ", keyword_model.similarity(word, cat))
        except Exception as e:
            try:
                #print("First exception: ", e)
                #cat=pre_trained_word2vec.most_similar_to_given(word, tags)
                value = pre_trained_word2vec.similarity(word, cat)
                if value>threshold:
                    if value>(multiplication_factor):
                        #print(word, cat, value)
                        conf[tag_to_idx.get(cat)] += value*(no_of_keywords-1)
                    conf[tag_to_idx.get(cat)]+=value
                #print("Pretrained: ", pre_trained_word2vec.similarity(word, cat))
            except Exception as e:
                if no_of_keywords>1:
                    no_of_keywords = no_of_keywords-1
                break
                    
        if cat == 'hollywood' and not word=='hollywood':
            if value>threshold:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value
        
        if cat == 'bollywood' and not word=='bollywood':
            if value>multiplication_factor:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value
        
        if cat in ('asian', 'disney', 'erotica', 'fairytales', 'thriller'):
            if value>multiplication_factor:
                conf[tag_to_idx.get(cat)] -= value*no_of_keywords
            else:
                conf[tag_to_idx.get(cat)] -= value
        
            #print("cat-word :", cat, word)
            value=1.0        
            if word in ('fairytale', 'fairytales', 'fairy', 'cinderella', 'cinderalla'):
                if 'fairytales' in tags:
                    tag_under_consideration = 'fairytales'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            elif word in ('erotica', 'porn', 'bdsm', 'lesbian', 'softcore', 'nudes'):
                if 'erotica' in tags:
                    tag_under_consideration = 'erotica'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            elif word in ('epcot', 'disneyland', 'disney', 'dw', 'walt'):
                if 'disney' in tags:
                    tag_under_consideration = 'disney'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            elif word in ('asian', 'chinese', 'japanese', 'china', 'japan'):
                if 'asian' in tags:
                    tag_under_consideration = 'asian'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            elif word in ('thriller', 'suspense', 'suspenseful'):
                if 'thriller' in tags:
                    tag_under_consideration = 'thriller'
                    conf[tag_to_idx.get(tag_under_consideration)] += value*no_of_keywords
            else:
                for tag_ in ['erotica', 'fairytales']:
                    if not word=='fiction':
                        try:
                            value = pre_trained_word2vec.similarity(tag_, word)
                            if value>0.505:
                                conf[tag_to_idx.get(tag_)] += value*no_of_keywords
                            elif value>0.45 and value<=0.505:
                                conf[tag_to_idx.get(tag_)] += value
                            elif not word =='orlando' and cat=='disney':
                                value = pre_trained_word2vec.similarity('disney', word)
                                if value>0.5 and value<0.6:
                                    conf[tag_to_idx.get('disney')] += value
                                elif value > 0.6:
                                    conf[tag_to_idx.get('disney')] += value*no_of_keywords
                        except Exception as e:
                            if no_of_keywords>1:
                                no_of_keywords -=1
                            break
    return conf, no_of_keywords


# In[31]:


def predict_category(keywords, top_categories, cat_conf):
    keywords = list(set(keywords))
    no_of_keywords = len(keywords)
    valid_keywords =  []
    value = 0
    for word in keywords:
        if word=='self':
            no_of_keywords -=1
            continue
        for cat in top_categories:
            if (cat=='workout' and word=='business') or (cat=='workout' and word=='marketing'):                
                continue
            if (cat=='education' and word=='business'):
                cat_conf[cat_to_idx.get('education')]+=0.2
            if (cat=='education' and word=='marketing'):
                cat_conf[cat_to_idx.get('education')]+=0.2
                               
            try:
                value = keyword_model.similarity(word, cat)
                valid_keywords.append(word)
                if ((cat=='workout' or cat=='astrology') and value>0.5) or (not (cat=='workout' or cat=='astrology') and value>0.2):
                    cat_conf[cat_to_idx.get(cat)]+=value
                elif cat==comedy or cat=='meditation':
                    print(mediation)
                    pass
            except Exception as e:
                try:
                    value = pre_trained_word2vec.similarity(word, cat)
                    valid_keywords.append(word)
                    if ((cat=='workout' or cat=='astrology') and value>0.5) or (cat=='comedy' and value>0.5) or (not (cat=='comedy' or cat=='workout' or cat=='astrology') and value>0.2):
                        cat_conf[cat_to_idx.get(cat)]+=value
                except Exception as e:
                    #print("Exception is: ", e)
                    if no_of_keywords>1:
                        no_of_keywords = no_of_keywords-1
                    break
    
    cat_conf_ = [value/no_of_keywords for value in cat_conf]
    cat_conf = np.array(cat_conf_)

    
    valid_keywords = list(set(valid_keywords))
    arr1 = np.sort(cat_conf)
    
    if 'comedy' in top_categories and arr1[-int(len(top_categories)/2)]<=cat_conf[cat_to_idx.get('comedy')]:
        try:
            if pre_trained_word2vec.similarity(pre_trained_word2vec.most_similar_to_given('comedy', valid_keywords), 'comedy')>0.5:
                cat_conf[cat_to_idx.get('comedy')]+=0.2
            else:
                cat_conf[cat_to_idx.get('comedy')]-=0.5
            #print(valid_keywords, pre_trained_word2vec.similarity(pre_trained_word2vec.most_similar_to_given('comedy', valid_keywords), 'comedy'))
        except:
            pass

    if 'comedy' in top_categories and 'stories' in top_categories and ('drama' in keywords or 'thriller' in keywords  or 'fiction' in keywords) and not 'comedy' in keywords:
        cat_conf[cat_to_idx.get('comedy')]=0.0
        cat_conf[cat_to_idx.get('stories')]+=0.2
        
    if 'kids' in top_categories and arr1[-int(len(top_categories)/2)]<=cat_conf[cat_to_idx.get('kids')] and len(intersection(sexual_words, keywords))>0:
        cat_conf[cat_to_idx.get('kids')]=0.0 
    
    return cat_conf


# In[32]:


idx_to_tag = {v: k for k, v in tag_to_idx.items()}
idx_to_cat = {v: k for k, v in cat_to_idx.items()}


# In[33]:


for term in tags:
    try:
        keyword_model.similar_by_word(term)
    except Exception as e:
        try:
            list_for_keyword_model.remove(term)
        except:
            pass
        
try:
    list_for_keyword_model.remove('fairytales')
except:
    pass


# In[ ]:





# In[34]:


prediction = []
actual_tags = []
predicted_category = []
rejected_categories = []
acc = 0
count = 0
skipped = 0
for i in range(df.shape[0]):
    try:
        conf = np.zeros(len(tags))
        keywords = []
        predicted_tags = []
        try:
            keywords = list(df['keyword'].iloc[i])
            
        except Exception as e:
            print(e)
            keywords = []
            
        categories_for_this = df['all_categories'].iloc[i]
        keywords.extend(categories_for_this)
        keywords = list(set(keywords))
        keywords = [key for key in keywords if key not in stop_words and len(key)>2]
        no_of_keywords = len(keywords)
        
        author = '-'
        try:
            author=df['author'].iloc[i]
            if isNaN(author) or len(author)==0:
                author = '-'
        except Exception as e:
            print(e)
            
        show_id = -1
        try:
            show_id=df['id'].iloc[i]
            if isNaN(show_id):
                show_id = -1
        except Exception as e:
            print(e)
            
        title = ''
        try:
            title=df['title'].iloc[i]
            if isNaN(title):
                title = -1
        except Exception as e:
            print(e)
            
        show_id = show_id.item()
        tag = author
        url = 'https://cmsdev.gaana.com/api/createNewTag'
        pload = {"tag_name": tag, "tag_type": 2}
        t = json.dumps(pload)
        r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
        id_no = int(r.text[r.text.rindex(' '):r.text.rindex("\"")].strip())
        tag_to_id_mapping[tag]=id_no
        
        
        if no_of_keywords <=2:
            predicted_tags = [a for a in keywords if a in tags]
            pred_cat = [a for a in predicted_tags if a in top_categories]
            if len(pred_cat):
                for a in pred_cat:
                    predicted_tags.remove(a)
                    
                
            if not len(pred_cat):
                try:
                    cat_conf = np.zeros(len(top_categories))
                    cat_conf = predict_category(keywords, top_categories, cat_conf)
                    pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
                except:
                    pred_cat = ['-']
                    
            rejected = []
                
            if len(pred_cat)==1 and pred_cat[0]=='workout':
                cat_conf =  predict_category(keywords, top_categories, cat_conf)
                pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
            
#             prediction.append(predicted_tags)
#             predicted_category.append(pred_cat)
#             rejected_categories.append(rejected)
            #continue
            
        else:
            for each_word in keywords:
                conf, no_of_keywords = predict_tag(each_word, conf, no_of_keywords)

            scores = [(score/no_of_keywords) for score in conf]
            scores = np.array(scores)

            max_idx = np.where(scores>0.5, 1, 0)
            
            predicted_tags =  [idx_to_tag.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
            predicted_tags = remove_outliers(predicted_tags, keywords)
            
            
        to_be_removed = []               
        for tag in copy.copy(predicted_tags):
            replaced = False
            if tag in list(split_to_original.keys()):
                original_list = split_to_original[tag]
                for each_word in original_list:
                    x = (re.sub('[^a-zA-Z0-9]', ' ', each_word)).split()
                    x.remove(tag)
                    if len(x)>0 and x[0] in predicted_tags:
                        predicted_tags.append(each_word)
                        replaced = True
                            
                if len(original_list)>0 and not tag in top_categories:
                    to_be_removed.append(tag)
                
        predicted_tags = [tag for tag in predicted_tags if not tag in to_be_removed]
        
        predicted_tags = [(re.sub('[-]', ' ', a)) for a in predicted_tags] 

        #keywords = [(re.sub('[-]', ' ', a)) for a in keywords] 
        pred_cat =[]  #If any tag is also a category, move that tag into the category list. 
        for tag in copy.copy(predicted_tags):
            if tag in top_categories:
                pred_cat.append(tag)
                predicted_tags.remove(tag)
        
        
        predicted_tags = list(set(predicted_tags))
            
        if 'kids' in predicted_tags and len(intersection(sexual_words, keywords))>0:
            predicted_tags.remove('kids')

#         if len(predicted_tags)==0:
#             predicted_tags = ['-']
            
        rejected = []
        
        if len(copy.copy(pred_cat))>=3 :
            cat_conf = np.zeros(len(top_categories))
            if 'astrology' in pred_cat and 'astrology' in keywords:
                cat_conf[cat_to_idx.get('astrology')]+=0.5
            if len(keywords)>0:
                cat_conf = predict_category(keywords, top_categories, cat_conf)
            try:
                cat_conf[cat_to_idx.get('workout')] = (cat_conf[cat_to_idx.get('workout')]>0.95).astype(int)
            except:
                print(e)
                pass
            
            rejected = copy.copy(pred_cat)
            positions_of_prediction = [cat_to_idx.get(a) for a in rejected]
            
            for i in range(len(cat_conf)):
                if i not in positions_of_prediction:
                    cat_conf[i]=0
            
            arr1 = np.sort(cat_conf)
            if arr1[-2]/arr1[-1]>0.9:
                threshold_position=2
            else:
                threshold_position=1
            threshold_position=2 #The number of tags you want to keep even after pruning
            max_idx = np.where(cat_conf>=arr1[-threshold_position], 1, 0)
            pred_cat =  [idx_to_cat.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
            for t in pred_cat:
                rejected.remove(t)
        
        for each_word in rejected:
            if each_word in pure_tags and each_word in keywords:
                predicted_tags.insert(0,each_word)
                
        redundant_tags = intersection(predicted_tags, exact_matches_only)
        
        if len(redundant_tags):
            tags_to_be_kept = intersection(redundant_tags, keywords)
            tags_to_be_removed = [a for a in redundant_tags if a not in tags_to_be_kept]
            if len(tags_to_be_removed):
                for a in tags_to_be_removed:
                    predicted_tags.remove(a)
            
        cat_conf = np.zeros(len(top_categories))
        
        if len(copy.copy(pred_cat))==0:
            cat_conf = predict_category(keywords, top_categories, cat_conf)
            arr1 = np.sort(cat_conf)
            threshold=0
            if arr1[-2]/arr1[-1]>0.9:
                threshold = arr1[-2]
            else:
                threshold = arr1[-1]
                
            max_idx = np.where(cat_conf>=threshold, 1, 0)
            pred_cat =  [idx_to_cat.get(idx) for idx in range(len(max_idx)) if max_idx[idx] >0 ]
            #predicted_cat = remove_outliers(predicted_cat, keywords)
        
        
        if len(pred_cat)==1 and pred_cat[0]=='workout':
            cat_conf =  predict_category(keywords, top_categories, cat_conf)
            pred_cat = [idx_to_cat.get(np.argmax(cat_conf))]
            
        pred_cat = list(set(pred_cat))
                
        if len(pred_cat) == 2 and 'health' in  pred_cat and 'entertainment' in pred_cat:
            pred_cat = ['entertainment']
            
        if len(pred_cat)<2:
            if len(intersection(motivation, predicted_tags))>0:
                pred_cat.append('motivation')
            elif len(intersection(meditation, predicted_tags))>0:
                pred_cat.append('meditation')
        
        for tag in copy.copy(predicted_tags):
            if tag in ignore_hyphen.keys():
                predicted_tags.append(ignore_hyphen[tag])
                predicted_tags.remove(tag)
                
        if 'fiction' in keywords:
            predicted_tags.insert(0, 'fiction')
                
        if author not in predicted_tags:
            predicted_tags.append(author)
                
#        print("Input words: ", keywords)
#        print("Predicted tags: ", predicted_tags)
#        print("Predicted Category: ", pred_cat)
                
        x = [tag_to_id_mapping.get(tag) for tag in predicted_tags]
        predicted_tag_ids  = ','.join(str(e) for e in x)
        
        x = [cat_to_id_mapping.get(cat) for cat in pred_cat]
        pred_cat_ids  = ','.join(str(e) for e in x)
        

        
        url = 'https://cmsdev.gaana.com/api/updateTagsInShow'
        pload = {
            "tags_id":predicted_tag_ids,
            "show_id":show_id,
            "tag_type":2
            }

        t = json.dumps(pload)
        r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
        
        url = 'https://cmsdev.gaana.com/api/updateTagsInShow'
        pload = {
            "tags_id":pred_cat,
            "show_id":show_id,
            "tag_type":1
            }
        
        t = json.dumps(pload)
        r = requests.post(url = url, data = t, headers={'content-type':'application/json'})
        #print(r.text)
        
        fields = [show_id, title, keywords, predicted_tags, [predicted_tag_ids], pred_cat, [pred_cat_ids], rejected]
        with open('Podcasts.csv','a+') as f:
            writer = csv.writer(f)
            writer.writerow(fields)
      
        
    except Exception as e:
        print("Second exception", e)
        print(show_id)
        


# In[35]:


print(len(prediction))
print(df.shape)

print(f'{(time.time()-start_time)/60:.2f} mins')


# In[ ]:





# In[ ]:





# In[ ]:




